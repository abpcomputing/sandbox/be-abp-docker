#!/bin/bash

# Mounting EOS
if [ `whoami` == 'root' ]
    then
    if mountpoint -q /eos/user/s
    then
       :
    else
        eosxd -ofsname=home-s
    fi
    if mountpoint -q /eos/user/e
    then
       :
    else
        eosxd -ofsname=home-e
    fi
    if mountpoint -q /eos/project/l
    then
       :
    else
        eosxd -ofsname=project-l
        export KRB5CCNAME=FILE:/tmp/krb5cc_0
        tmux
    fi
fi


# activating conda
conda activate $HOME/env/
alias jupy='jupyter lab --no-browser --ip=0.0.0.0 --allow-root'

git config --global user.name "Guido Sterbini"
git config --global user.email "guido.sterbini@cern.ch"
