# Introduction

We would like to explore the potential of docker in our community.
This repository is there a first attempt in this direction.

## Main use case
I have a Mac laptop and I would like to work locally on my Mac in a UNIX system for the following reasons:
- to work directly on lxplus can be slow (node dependent). The advantage of lxplus is that it mounts `afs`, `eos`, `cvmfs`... and the HTCondor interface.  
- work on SWAN can be slow and it does not mount `afs` - this can be problematic for our MAD-X mask.
- python C-compiled package can have difficulties to be maintained for Mac/Win, e.g., `cpymad` does not work properly on Mac (I manage to install it on Mad and run small lattice, but when I try the larger ones, namely LHC, it fails). I started to use a simple docker as a workaround (the same I used for CAS/JUAS courses), typically I use `jupyter lab` and my experience in term of performance and stability is  positive.
- I would like to mount `eos` and `afs` without suffering to much
- I would like that the possibility to share my setup not only via a set of wikis but also with a press-one-button solution. With this spirit, one could prepare and share several conda `env`, e.g,. to track particles, to access NXCALS,...
- one can also couple VSCode directly on the container.

For the moment, I am considering that you have already installed in your Mac (localhost) `afs` - it is quite easy to do so, I tested the docker only on Catalina.
You do not need to install `eos` on your Mac. This is installed inside the container (thanks to the help of Fabio Luchetti).  

# To run the docker

To lauch the docker sterbini/test

```bash
docker run -it -u 0 -v /afs:/afs -v ~:/home/jovyan/local_host_home -p 8888:8888 --rm --cap-add SYS_ADMIN --device /dev/fuse gitlab-registry.cern.ch/abpcomputing/sandbox/be-abp-docker:$commit_tag
```

where you can find the available $commit_tag at https://gitlab.cern.ch/abpcomputing/sandbox/be-abp-docker/container_registry/9443. Probably you want the latest tag. 

**At the time of the writing (2021.08.23) the latest tag was 0cd55ff2.**

In case you have Debian, Kostantinos Paraschou pointed out that one needs to add the argument `--security-opt apparmor:unconfined`, e.g.:
```bash
docker run -it -u 0 -v /afs:/afs -v ~/:/home/jovyan/local_host_home -p 8888:8888 --rm --cap-add
SYS_ADMIN --device /dev/fuse --security-opt apparmor:unconfined gitlab-
registry.cern.ch/abpcomputing/sandbox/be-abp-docker:288bbf2a
```



In case you want to mount a local folder, this has to be **shared** with Docker Docker (I did it with Docker Desktop on my Mac)
and then use the `-v` option

```bash
docker run -it -u 0 -v /afs:/afs -v ~:/home/jovyan/local_host_home -p 8888:8888 --rm --cap-add SYS_ADMIN --device /dev/fuse gitlab-registry.cern.ch/abpcomputing/sandbox/be-abp-docker:288bbf2a
```

The volumes you mount are very important, since only the information you save there will survive after you exit the container. 

When the container is starting will try to run the file `/home/jovyan/local_host_home/docker_start.sh`. You can customize it accordingly to your need. You find a template of the file in `/home/jovyan/docker_start.sh`. So please copy it on `/home/jovyan/local_host_home`.

You can run a jupyter notebook server with `jupy`.

When you are in the docker you have to do:

``` bash
kinit $YOUR_NICE_LOGIN_NAME
```

### To connect to NXCALS

Open a terminal on your laptop and copy the `run_sshuttle.sh` file from the container home to your local_host_home (we assume is `~`).

From the local_host run then the `~/run_sshuttle.sh connect`.

Open a new tab in your terminal and launch the container.
In the container do 
```
kinit sterbini 
~/local_host_home/run_sshuttle.sh connect
python test_pytimber.py
```
# To make the docker

To build the docker

```bash
docker build . --build-arg username=jovyan -t $IMAGE_NAME
```

where $IMAGE_NAME can be for example `sterbini/test`

## To export the X-graphics on Mac
From https://medium.com/@mreichelt/how-to-show-x11-windows-within-docker-on-mac-50759f4b65cb

- Install the latest XQuartz X11 server and run it
- Activate the option ‘Allow connections from network clients’ in XQuartz settings
- Quit & restart XQuartz (to activate the setting)

On the local host (Mac) do 

```bash
xhost + 127.0.0.1
```

And when you run the docker by adding 

```bash
-e DISPLAY=host.docker.internal:0
```

to the `docker run` that is

```bash
docker run --rm -e DISPLAY=host.docker.internal:0 -p 8889:8888 -ti --cap-add SYS_ADMIN --device /dev/fuse -e GRANT_SUDO=yes gitlab-registry.cern.ch/abpcomputing/sandbox/be-abp-docker:$TAG
```

then you can test from the container
```bash
xclock
```

## Some simple operations with Docker

To see all running containers you can do (https://phoenixnap.com/kb/how-to-ssh-into-docker-container)

```bash
docker ps
```

then you can attach from another local-host terminal to the container

```
docker attach $CONTAINER_ID
```

the $CONTAINER_ID is the list of the docker processes - you can also use the $CONTAINER_NAME to attach it.

If you use `docker attach` you are attaching the very same terminal.
If you want to open a new terminal in the container you can do
```bash
docker exec -it $CONTAINER_ID /bin/bash
```

## Containers renaming, stopping and reattaching.
You can use '-n' option to name a container (no other containers with that name should existin in the Docker Host).
Then
```bash
docker run --name happy_newton -e DISPLAY=host.docker.internal:0 -p 8889:8888 -p 8822:22 -ti --cap-add SYS_ADMIN --device /dev/fuse -e GRANT_SUDO=yes -v /afs:/afs $MY_IMAGE:$MY_TAG
```

One you exit the container, it will not be removed but it will stopped. You can remove it automatically if you add `--rm` option
```
docker run --rm --name happy_newton -e DISPLAY=host.docker.internal:0 -p 8889:8888 -p 8822:22 -ti --cap-add SYS_ADMIN --device /dev/fuse -e GRANT_SUDO=yes -v /afs:/afs $MY_IMAGE:$MY_TAG
```

It you do not remove the container when you exit it you can restart it and reattach it. For example:
```
docker container start happy_newton
docker attach happy_newton
```

You can also remove a  
```
container rm happy_newton
``` 
