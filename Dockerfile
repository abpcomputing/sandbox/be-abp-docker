# reference: https://hub.docker.com/_/ubuntu/
FROM ubuntu:focal-20210325

ARG DEBIAN_FRONTEND=noninteractive

LABEL maintainer="Guido Sterbini <guido(.)sterbini at cern.ch>"

###############################################################################
# Install utilities
###############################################################################

RUN apt-get -qq update \
 && apt-get -yqq install apt-utils nano curl lsb-release gnupg2 krb5-user \
 && apt-get -yqq clean

###############################################################################
# Install EOS, thanks to @note(faluchet)
# following somehow also 
# https://cern.service-now.com/service-portal?id=kb_article&n=KB0006614
###############################################################################

COPY etc/eos /etc/eos
COPY krb5.conf /etc/krb5.conf
COPY create_eos_folders.sh /root/create_eos_folders.sh

RUN echo "deb [arch=$(dpkg --print-architecture)] \
    http://storage-ci.web.cern.ch/storage-ci/debian/xrootd/ \ 
    $(lsb_release -cs) release" \
    | tee -a /etc/apt/sources.list.d/cerneos-client.list > /dev/null \
 && echo "deb [arch=$(dpkg --print-architecture)] \
    http://storage-ci.web.cern.ch/storage-ci/debian/eos/citrine/ \
    $(lsb_release -cs) tag" \
    | tee -a  /etc/apt/sources.list.d/cerneos-client.list > /dev/null \
 && curl -sL http://storage-ci.web.cern.ch/storage-ci/storageci.key \
    | apt-key add - \
 # @note(faluchet): as of eos version 4.8.40, 
 # we need to pin xrootd version 4.12.8. Will be lifted in the future
 && echo "Package: xrootd* libxrd* libxrootd*\nPin: \
    version 4.12.8\nPin-Priority: 1000" > /etc/apt/preferences.d/xrootd.pref \
 && apt-get -qq update \
 && apt-get -yqq install eos-client eos-fusex \
 && apt-get -yqq clean \
 && chmod 0755 /root/create_eos_folders.sh \
 && /root/create_eos_folders.sh

###############################################################################
# Installing AFS in the docker (not working)
# the main problem is related I think to docker
# https://stackoverflow.com/questions/53383431/how-to-enable-systemd-on-dockerfile-with-ubuntu18-04
# the present policy is to install it on the host_machine and share the volume with the docker
# e.g., http://abpcomputing.web.cern.ch/guides/openafs/
# ideally also for eos 
###############################################################################
# RUN apt-get install -y software-properties-common \
#  &&  apt-get update \
#  &&  add-apt-repository ppa:openafs/stable \
#  &&  apt-get update \ 
#  &&  apt-get upgrade \
#  &&  apt install -y openafs-client openafs-modules-dkms openafs-krb5 krb5-config

###############################################################################
# Some minimal packages
###############################################################################
RUN apt-get -qq update \
 && apt-get -yqq install x11-apps sudo wget git tmux vim meld openssh-server \
    build-essential cmake python-dev python3-dev openjdk-8-jdk sshuttle graphviz tree\
 && apt-get -yqq clean

###############################################################################
# MAD-X installation (last releases)
###############################################################################
RUN wget http://madx.web.cern.ch/madx/releases/last-rel/madx-linux64-gnu \
 && mv madx-linux64-gnu /usr/bin/madx \
 && chmod +x /usr/bin/madx

###############################################################################
# Install miniconda
# https://hub.docker.com/r/continuumio/miniconda/dockerfile
# from https://towardsdatascience.com/conda-pip-and-docker-ftw-d64fe638dc45
###############################################################################

SHELL [ "/bin/bash", "--login", "-c" ]

# Create a non-root user
ARG username=jovyan
ARG uid=1000
ARG gid=100
ENV USER $username
ENV UID $uid
ENV GID $gid
ENV HOME /home/$USER

RUN adduser --disabled-password \
    --gecos "Non-root user" \
    --uid $UID \
    --gid $GID \
    --home $HOME \
    $USER

COPY environment.yml requirements.txt /tmp/
COPY postBuild.sh /usr/local/bin/postBuild.sh
COPY entrypoint.sh /usr/local/bin/

RUN chown $UID:$GID /tmp/environment.yml /tmp/requirements.txt \
 && chown $UID:$GID /usr/local/bin/postBuild.sh \
 && chmod u+x /usr/local/bin/postBuild.sh \
 && chown $UID:$GID /usr/local/bin/entrypoint.sh \
 && chmod u+x /usr/local/bin/entrypoint.sh

USER $USER

ENV MINICONDA_VERSION 4.7.12
ENV CONDA_DIR $HOME/miniconda3
ENV PATH=$CONDA_DIR/bin:$PATH

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-$MINICONDA_VERSION-Linux-x86_64.sh -O ~/miniconda.sh \
 && chmod +x ~/miniconda.sh \
 && ~/miniconda.sh -b -p $CONDA_DIR \
 && rm ~/miniconda.sh \
 # make conda activate command available from /bin/bash --login shells
 && echo ". $CONDA_DIR/etc/profile.d/conda.sh" >> ~/.profile \
 # make conda activate command available from /bin/bash --interative shells
 && conda init bash \
 # create a project directory inside user home
 && mkdir $HOME/local_host_home

WORKDIR $HOME

# build the conda environment
ENV ENV_PREFIX $HOME/env
RUN conda update --name base --channel defaults conda \
 && conda env create --prefix $ENV_PREFIX --file /tmp/environment.yml --force \
 && conda clean --all --yes \
 # run the postBuild script to install any JupyterLab extensions
 && conda activate $ENV_PREFIX \
 && /usr/local/bin/postBuild.sh \
 && conda deactivate

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN conda activate $ENV_PREFIX \
 && pip install -r /tmp/requirements.txt \
 && pip install pyNAFF \
 && git clone https://github.com/SixTrack/pysixtrack.git \
 && cd pysixtrack \
 && pip install -e . \
 && cd .. \
 && git clone https://github.com/SixTrack/sixtracktools.git \
 && cd sixtracktools \
 && pip install -e . \
 && cd .. \
 && git clone https://github.com/SixTrack/sixtracklib.git \
 && cd sixtracklib \
 && mkdir build \
 && cd build \
 && cmake .. \
 && make \
 && cd ../python \
 && pip install -e . \
 && cd ../.. \
 && git clone https://github.com/PyCOMPLETE/FillingPatterns.git \
 && pip install ./FillingPatterns \
 && python -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch pytimber \
 && python -m cmmnbuild_dep_manager resolve \
 && conda deactivate


COPY docker_start.sh $HOME/docker_start.sh
COPY run_sshuttle.sh $HOME/run_sshuttle.sh
COPY test_pytimber.py $HOME/test_pytimber.py
RUN echo "source /home/"$USER"/local_host_home/docker_start.sh" >> ~/.bashrc

USER $USER
CMD ["/bin/bash" ]

# FOR BUILDING
# docker build . --build-arg username=jovyan -t sterbini/test
# FOR RUNNING
# docker run -it -u 0 -v /afs:/afs -v ~:/home/jovyan/local_host_home -p 8889:8888 --rm --cap-add SYS_ADMIN --device /dev/fuse sterbini/test
# TO LAUNCH jupyterlab
# jupyter lab --no-browser --ip=0.0.0.0 --allow-root
