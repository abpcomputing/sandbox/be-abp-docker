import pytimber  
print("pytimber imported.")
ldb=pytimber.LoggingDB(source='nxcals')  # Backport API
print("LoggiggDB connected.")

nxcals = pytimber.NXCals()
spark = nxcals.spark
print("spark variable defined")

var = "AUTOMATICSCAN:IP"
t1 = "2018-09-01 00:00:00.000"
t2 = "2018-09-01 23:00:00.000"

print(f'---- > Query var={var}, in [{t1},{t2}]')
ds = nxcals.DataQuery.builder(spark).byVariables() \
        .system('CMW') \
        .startTime(t1).endTime(t2) \
        .variable(var).build()

ds.show()
print('Exiting...')
import jpype as jp
System = jp.JClass("java.lang.System")
System.exit(0)

