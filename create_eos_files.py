import string
for ii in list(string.ascii_lowercase):
    with open(f"tmp/fuse.home-{ii}.conf", "w") as text_file:
        text_file.write('{'+f'"name":"home-{ii}",' + \
                        f'"hostport":"eoshome-{ii}.cern.ch",'+ \
                        f'"remotemountdir":"/eos/user/{ii}/",'+ \
                        f'"localmountdir":"/eos/user/{ii}/"' +'}')
    with open(f"tmp/fuse.project-{ii}.conf", "w") as text_file:
        text_file.write('{'+f'"name":"project-{ii}",' + \
                        f'"hostport":"eosproject-{ii}.cern.ch",'+ \
                        f'"remotemountdir":"/eos/project/{ii}/",'+ \
                        f'"localmountdir":"/eos/project/{ii}/"' +'}')
